pop3browser
===========


Purpose:
--------

Since SPAM and mail worms cause more and more traffic it becomes more
and more annoying to get these mails via a low bandwidth connection
(e.g. 56k modem). pop3browser allows to have a look at a pop3 mailbox
and delete unwanted mails before downloading it.


Usage:
------

pop3browser welcomes you with a short copyright note and a command
line prompt. The settings for a mailbox (host, password etc.) have to
specified in a configuration file (similar to fetchmail).
pop3browser understands the following commands:

hosts        : list available hosts specified in .pop3browserrc
login n      : log into host no. n, use 'hosts' to get a list of available hosts
apop n       : same as login but uses APOP 
close        : delete messages marked for deletion and close current connection
list         : list sizes and message numbers of the mails on the account
show n       : show header and some body lines of mail number n
del(ete) n   : delete mail number n
undel(ete) n : undelete mail number n
kill expr    : delete mails matching expr in header
unkill expr  : undelete mails matching expr in header
quit         : delete messages marked for deletion and quit program
exit         : same as quit
help         : print a command summary


Configuration File:
-------------------

The definitions for a mailbox are specified in .pop3browserrc. One
line per mailbox. Lines starting with # are ignored.
Each definition must be in the following form:
hostname <tab or space> userid <tab or space> password
For example:
#definition for bar@pop.foo.com
pop.foo.com	bar	s3cr3t
#end of definition

The file .pop3browserrc must only be readable for the owning
user. pop3browser corrects other file modes automatically.
 





